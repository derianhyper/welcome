from pydal import DAL, Field

db = DAL('sqlite://storage.sqlite')




db.define_table('tasks', Field('title', type='string'), 
                        Field('description', type='string'),
                        Field('date', type='date'))